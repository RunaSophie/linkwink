<?php
namespace database;

function connect($config) : \PDO {
    
    // create a new PDO connection with the saved configuration
    $dbsrv = $config['databaseServer'];
    $dbport = $config['databasePort'];
    $dbname = $config['databaseName'];
    $dsn = "mysql:host=$dbsrv;port=$dbport;dbname=$dbname";
    $username = $config['databaseUsername'];
    $password = $config['databasePassword'];
    
    return new \PDO($dsn, $username, $password);
}
