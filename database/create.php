<?php
namespace database;

function createTables(\PDO $pdo) {
    
    // Create users table
    $sql = 'CREATE TABLE IF NOT EXISTS wink (' .
        'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, '.
        'winkname VARCHAR(50) NOT NULL UNIQUE, '.
        'winktitle VARCHAR(255) NOT NULL UNIQUE, '.
        'password VARCHAR(255) NOT NULL, '.
        'active BOOL NOT NULL, '.
        'gradientDir INT NOT NULL, '.
        'textcol INT NOT NULL, '.
        'loginFails INT NOT NULL DEFAULT 0);';
    $pdo->query($sql);
    
    // Create gradient colors table
    $sql = 'CREATE TABLE IF NOT EXISTS winkgradientcol (' .
        'winkId INT NOT NULL, '.
        'orderNum INT NOT NULL, '.
        'color INT NOT NULL, '.
        'FOREIGN KEY (winkId) REFERENCES wink(id));';
    $pdo->query($sql);
    
    // Create credentials table
    $sql = 'CREATE TABLE IF NOT EXISTS link (' .
        'id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, '.
        'winkId VARCHAR(50) NOT NULL, '.
        'link VARCHAR(255) NOT NULL, '.
        'description VARCHAR(255) NOT NULL, '.
        'FOREIGN KEY (winkId) REFERENCES wink(id));';
    $pdo->query($sql);
}