<?php
echo 'test';
error_reporting(E_ALL);
echo 'test2';
require_once __DIR__ . '/database/Database.php';
echo 'test3';
$config = json_decode(file_get_contents(__DIR__ . '/manage/config.json'), true);

if (isset($_GET['winkname'])) {
  try {
    $pdo = database\connect($config);
    $sql = 'SELECT * FROM wink LEFT JOIN link ON wink.id = link.winkId WHERE wink.winkname = :wn AND wink.active = 1';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':wn' => $_GET['winkname']]);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $sql = 'SELECT * FROM wink LEFT JOIN winkgradientcol ON wink.id = winkgradientcol.winkId '.
        'WHERE wink.winkname = :wn AND wink.active = 1 '.
        'ORDER BY orderNum asc';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':wn' => $_GET['winkname']]);
    $gradientcols = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $mode = "displayWink";
  } catch (PDOException $ex) {
    $mode = "notFound";
  }
} else {
  $mode = "welcomePage";
}
?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <?php
    switch ($mode) {
      case "displayWink":
    ?>
    <title><?php echo $result[0]['winktitle']; ?></title>
    <style type="text/css">
      body {
        background-image: linear-gradient(<?php
          echo $result[0]['gradientDir'] . 'deg';
          foreach ($gradientcols as $color) {
            echo ', #' . str_pad($color['color'], 6, '0', STR_PAD_LEFT);
          }
        ?>);
      }
    </style>
    <?php
        break;
      case "welcomePage":
    ?>
    <title>LinkWink</title>
    <?php
        break;
      case "notFound":
    ?>
    <title>LinkWink not found</title>
    <?php 
    }
    ?>
    <link rel="stylesheet" href="style.css" />
    <link rel="icon" type="image/svg+xml" href="eye.svg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <?php
    switch ($mode) {
      case "displayWink":
    ?>
    <div class="container">
      <h1><?php echo $config['linkWinkName'] ?></h1>
      <?php foreach ($result as $row) { ?>
      <a class="link" href="<?php echo $row['link']; ?>"><?php echo $row['description']; ?></a>
      <?php }
      if ($stmt->rowCount() == 0) { ?>
      This LinkWink is empty!
      <?php } ?>
    </div>
    <?php
        break;
      case "welcomePage":
    ?>
    <div class="container">
      <h1>LinkWink</h1>
      <a class="link" href="manage/request.php">Click here to create a new LinkWink!</a>
    </div>
    <?php
        break;
      case "notFound":
    ?>
    <div class="container">
      <h1>LinkWink not found!</h1>
      <p>This LinkWink does not exist or is not active.</p>
      <a class="link" href="manage/request.php">Create a new LinkWink</a>
      <a class="link" href="manage/login.php">Edit your LinkWink</a>
    </div>
    <?php 
    }
    ?>
  </body>
</html>