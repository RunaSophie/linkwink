<?php
namespace manage;

session_start();

error_reporting(E_ALL);

$config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);
$errors = [];
$loggedIn = false;
$refresh = false;
$databaseSuccess = false;

if (isset($_POST['login'])) {
    if (!isset($_POST['username']) || !isset($_POST['password'])) {
        $errors[] = 'Please enter a username and password';
        $loggedIn = false;
    } else if ($_POST['username'] != $config['adminUsername']
        || !password_verify($_POST['password'], $config['adminPassword'])) {

        $errors[] = 'Username or Password incorrect';
        $loggedIn = false;
    } else {
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['password'] = $_POST['password'];
        $loggedIn = true;
    }
}

if (isset($_SESSION['username']) && isset($_SESSION['password'])) {
    if ($_SESSION['username'] != $config['adminUsername']
        || !password_verify($_SESSION['password'], $config['adminPassword'])) {
        
        $errors[] = 'Username or Password incorrect';
        $loggedIn = false;
    } else {
        $loggedIn = true;
    }
}

if ($loggedIn) {
    if (isset($_POST['update'])) {
        $config['adminUsername'] = $_POST['username'];
        $config['adminPassword'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $config['databaseServer'] = $_POST['dbserver'];
        $config['databasePort'] = $_POST['dbport'];
        $config['databaseName'] = $_POST['dbname'];
        $config['databaseUsername'] = $_POST['dbusername'];
        $config['databasePassword'] = $_POST['dbpassword'] == '' ? null : $_POST['dbpassword'];
        $configToWrite = json_encode($config, JSON_PRETTY_PRINT);
        $result = file_put_contents('./config.json', $configToWrite);
        $refresh = true;
    } else {
        try {
            $pdo = \database\connect($config);
            \database\createTables($pdo);
            $stmt = $pdo->prepare('SELECT * FROM wink');
            $stmt->execute();
            $allwinks = $stmt->fetchAll();
            $databaseSuccess = true;
        } catch (\PDOException $ex) {
            $errors[] = 'Something went wrong with the database connection...<br />' . $ex->getMessage();
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en-ch">
  <head>
    <title>Configuration | LinkWink | RunaFocaccia</title>
    <link rel="stylesheet" href="omega.css" />
    <link rel="icon" type="image/svg+xml" href="../wink.svg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <nav class="navbar">
      <div class="navbar-element-disabled navbar-element-image">
        <img src="../wink.svg" height="30px" />
        <span style="padding-left: 10px; padding-right: 5px;">LinkWink</span>
      </div>
      <a href=".." target="_blank" class="navbar-element">
        Preview
      </a>
      <a href="." class="navbar-element">
        Management
      </a>
      <a href="./logout.php" class="navbar-element">
        Log out
      </a>
    </nav>
    <div class="container">
      <h1><?php if ($loggedIn) { echo 'Configuration'; } else { echo 'Login'; } ?></h1>
      <?php if ($errors != []) { ?>
        <ul class="error-list">
          <?php foreach ($errors as $error) {
            echo "<li>" . htmlentities($error) . "</li>\n";
          } ?>
        </ul>
      <?php }
      if ($loggedIn) {
      ?>
      	<form action="<?= $_SERVER["PHP_SELF"] ?>" method="POST">
      	  <h2>Administrator Login</h2>
          <div class="form-element">
            <label for="username" class="form-label">Username</label><br />
            <input type="text" class="form-input" id="username" name="username" value="<?= $_SESSION['username'] ?>" />
          </div>
          <div class="form-element">
            <label for="password" class="form-label">Password</label><br />
            <input type="text" class="form-input" id="password" name="password" value="<?= $_SESSION['password'] ?>" />
          </div>
          <h2>Database Server Settings</h2>
          <div class="form-element">
            <label for="dbserver" class="form-label">Server Address</label><br />
            <input type="text" class="form-input" id="dbserver" name="dbserver" value="<?= $config['databaseServer'] ?>" />
          </div>
          <div class="form-element">
            <label for="dbport" class="form-label">Server Port</label><br />
            <input type="number" class="form-input" id="dbport" name="dbport" value="<?= $config['databasePort'] ?>" />
          </div>
          <div class="form-element">
            <label for="dbname" class="form-label">Schema Name</label><br />
            <input type="text" class="form-input" id="dbname" name="dbname" value="<?= $config['databaseName'] ?>" />
          </div>
          <div class="form-element">
            <label for="dbusername" class="form-label">Username</label><br />
            <input type="text" class="form-input" id="dbusername" name="dbusername" value="<?= $config['databaseUsername'] ?>" />
          </div>
          <div class="form-element">
            <label for="dbpassword" class="form-label">Password</label><br />
            <input type="text" class="form-input" id="dbpassword" name="dbpassword" value="<?= $config['databasePassword'] ?>" />
          </div>
          <div class="form-element">
            <button type="submit" class="button-submit" name="update">Update Config!</button>
          </div>
        </form>
        <?php
        if ($databaseSuccess) {
          foreach ($allwinks as $wink) {
            $stmt = $pdo->prepare('SELECT * FROM link WHERE winkId = :wid');
            $stmt->execute([':wid' => $wink['id']]);
            $allLinksOfWink = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            ?>
          	  <input type="checkbox" class="linklist-checkbox" id="linklist-cb-<?php echo htmlentities($wink['winkname']); ?>">
          	  <div class="linklist">
          	    <h3 class="linklist-title"><label for="linklist-cb-<?php echo htmlentities($wink['winkname']); ?>">
          	      <?php echo htmlentities($wink['winkname']); ?>
          	    </label></h3>
          	    <form action="./modify-admin.php" method="GET"
          	          class="linklist-content flex-horizontal<?php echo $wink['active'] == 1 ? ' bg-disabled' : ''; ?>">
          	      <div class="flex-item-large">Name: <?php echo htmlentities($wink['winkname']); ?></div>
          	      <div class="flex-item-medium">Status: <?php echo $wink['active'] == 1 ? 'Active' : 'Inactive'; ?></div>
          	      <div class="flex-item-small">
          	      	<?php if ($wink['active'] == 1) { ?>
          	      	  <button type="submit" class="button-delete" name="lock" value="<?php echo $wink['id']; ?>">
          	      	    <img src="../locked.svg" alt="Lock" height="20px" />
          	      	  </button>
          	      	<?php } else { ?>
          	      	  <button type="submit" class="button-new" name="unlock" value="<?php echo $wink['id']; ?>">
          	      	    <img src="../unlocked.svg" alt="Unlock" height="20px" />
          	      	  </button>
                      <button type="submit" class="button-delete" name="delete" value="<?php echo $wink['id']; ?>">
          	            <img src="../delete.svg" alt="Delete" height="20px" />
       	              </button>
          	      	<?php } ?>
          	      </div>
          	    </form>
                <?php
                  foreach ($allLinksOfWink as $link) {
                    ?>
                    <div class="linklist-content flex-horizontal">
                    </div>
                    <?php
                  }
                ?>
          	  </div>
        	<?php
          }
        ?>
          
        <?php
        } else {
        ?>
        <?php
        }
        ?>
      <?php
      } else {
      ?>
      	<form action="<?= $_SERVER["PHP_SELF"] ?>" method="POST">
          <div class="form-element">
            <label for="adminUsername" class="form-label">Username</label><br />
            <input type="text" class="form-input" id="adminUsername" name="adminUsername" value="<?= $_SESSION['username'] ?>" />
          </div>
          <div class="form-element">
            <label for="adminPassword" class="form-label">Password</label><br />
            <input type="password" class="form-input" id="adminPassword" name="adminPassword" />
          </div>
          <div class="form-element">
            <button type="submit" class="button-submit" name="login">Login!</button>
          </div>
        </form>
      <?php
      }
      ?>
    </div>
    <?php if ($refresh) { ?>
    <script>
    window.location = window.location.href;
    </script>
    <?php } ?>
  </body>
</html>



