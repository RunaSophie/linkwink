<?php
namespace manage;

$config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);
$errors = [];

try {
    $pdo = \database\connect($config);
} catch (\PDOException $ex) {
    $errors[] = 'Something went wrong with the database connection...<br />' . $ex->getMessage();
}


?>