<?php
namespace manage;

session_start();
session_destroy();
header("Location: ./login.php?logoutSuccess");
?>