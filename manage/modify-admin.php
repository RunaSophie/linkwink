<?php
namespace manage;


$config = json_decode(file_get_contents(__DIR__ . '/config.json'), true);
$errors = [];
$loggedIn = false;
$databaseSuccess = false;


if (isset($_SESSION['username']) && isset($_SESSION['password'])) {
    if ($_SESSION['username'] != $config['adminUsername']
        || !password_verify($_SESSION['password'], $config['adminPassword'])) {

        $loggedIn = false;
    } else {
        $loggedIn = true;
    }
}


if ($loggedIn) {
    try {
        $pdo = \database\connect($config);
        
        if (isset($_POST['lock'])) {
            $stmt = $pdo->prepare('UPDATE wink SET active = FALSE WHERE id = :id');
            $stmt->execute([':id' => $_POST['lock']]);
        } else if (isset($_POST['unlock'])) {
            $stmt = $pdo->prepare('UPDATE wink SET active = TRUE WHERE id = :id');
            $stmt->execute([':id' => $_POST['unlock']]);
        } else if (isset($_POST['delete'])) {
            $stmt = $pdo->prepare('DELETE FROM wink WHERE id = :id');
            $stmt->execute([':id' => $_POST['delete']]);
        }
        
        $databaseSuccess = true;
    } catch (\PDOException $ex) {
        $errors[] = 'Something went wrong with the database connection...<br />' . $ex->getMessage();
    }
} else {
    http_response_code(401);
}

header("Location: config.php");

?>